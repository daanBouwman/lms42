from ..app import db, app
import datetime
from ..utils import generate_password
import flask


class TrialDay(db.Model):
    date = db.Column(db.Date, primary_key=True)

    slots = db.Column(db.Integer, nullable=False, default=2)


class TrialStudent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    secret = db.Column(db.String, nullable=False, default=generate_password)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)

    invitation_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    trial_day_date = db.Column(db.Date, db.ForeignKey('trial_day.date'), nullable=True)
    trial_day = db.relationship("TrialDay", backref="students")

    @property
    def url(self):
        return f"{flask.request.url_root}trial/{self.id}/{self.secret}"
