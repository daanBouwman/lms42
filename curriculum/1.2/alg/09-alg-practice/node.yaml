name: Practice exam
description: Solve an algorithmic problem.
days: 1
type: hand-in
miller: kh
goals:
    unittests: 1.5
    alg_impl: 1
    alg_sel: 1
    datastructures: 1
    recursion: 1
    backtracking: 1
    graphs: 1
    sorted_lists: 1

assignment1:
    "Solve the maze!":
        - |
            Today you'll be doing some serious maze solving. We're talking about mazes with **1000**x**1000** squares!

            Here's a relatively small example, of a maze of size **24**x**24**:

            <img src="24x24.png">

            The red squares are blockades - we cannot pass through. All other squares have a 'price'. This is what you would need to 'pay' to pass through a square. This particular maze has already been solved: the green path shows the 'cheapest' path, meaning the total price of all squares that the path goes through is as low as possible.

            Functions for generating the maze and displaying it have been provided in `jumpmaze.py`. Implementing the `solve(maze)` function that calculates the cheapest path is your job!

        - Maze structure: |
            A `maze` is given as a two-dimensional list, where `maze[y]` is a list that represents a row and `maze[y][x]` is a number that represents a single square. If the number is `0`, that square is a blockade. Otherwise, the number is the price for passing through that square. An example:
            ```python
            square = [
                [10,  5,  8, 20],
                [ 0,  0,  5,  6],
                [ 2, 11, 13, 17],
                [ 1,  4,  0,  9],
            ]
            ```
            The cheapest path would be:
            ```python
            [(0,0), (0,1), (0,2), (1,2), (1,3), (2,3), (3,3)]
            ```

        - Expected output:
            - Running `python jumpmaze.py` in the console will call `solve()` on a couple of demo mazes that have been provided. We have also provided you with a file containing the output you would expect once you have completed your solver, in a file appropriately named `expected-output.txt`. Opening this file is your editor will not be very helpful as it contains *terminal control characters* for setting colors, which your editor will display as weird characters. Instead type `cat expected-output.txt` in the console to see the colored output. You can use shift-PgUp and shift-PgDn to scroll back.
        
        - Cheapest path finding for demo mazes up to 24x24:
            text: If you can't make your algorithm fully work, demonstrating steps in the right direction can get you some points.
            code: 0.3
            0: Not much.
            1: Total path cost calculations are mostly right, no reverse path finding.
            2: Total path cost calculations are correct, but reverse path finding does not work.
            3: Cheapest path is found in some cases, or another correct path is found in all cases. 
            4: Cheapest path is found in all cases.

        - Cheapest path finding for larger demo mazes, within 1 minute each:
            text: If you can't make your algorithm fully work, demonstrating steps in the right direction can get you some points.
            code: 0.3
            0: Not much.
            1: Total path cost calculations are mostly right, no reverse path finding.
            2: Total path cost calculations are correct, but reverse path finding does not work.
            3: Cheapest path is found in some cases, or another correct path is found in all cases. 
            4: Cheapest path is found in all cases.

        - Unit tests:
            text: |
                Create a file `jumpmaze_test.py` and use it to write unit tests for all of the functions you write for this assignment. Aim for full coverage of all branches of your code.
            0: No (useful) tests.
            1: 25% branch coverage.
            2: 50% branch coverage, many spurious tests.
            3: 75% branch coverage, some spurious test or no logical split.
            4: Full coverage, only useful tests, logically split between test functions.


        - Hints: |
            - You will want to split up the work that needs to be done into multiple functions, for readability as well as testability.
            - We recommend that you use Test-driven development (TDD), creating unit tests before you implement the tested functionality, starting out with small and simple examples to test.
            - Finding a path usually consists of two steps. After the first step, you'll know the total cost of the cheapest path. Then you'll work backwards to find the squares that are in the part.


    "And.... teleport!":
        - |
            To make things even more interesting, we'll now try to solve the mazes with one extra rule: when two squares have the same price value, a path can 'teleport' between them. Take this square for instance:
                
            ```python
            square = [
                [1, 2, 3],
                [4, 5, 1],
                [6, 8, 9]
            ]
            ```

            The cheapest path would be `[(0,0), (1,2), (2,2)]`, because we can jump between the `1`s.

        - 
            text: Implement your solution in `solve_with_teleports(maze)`, reusing as much of your earlier work as you can. Solving a large maze should not take more than a minute.
            code: 0.3
            0: Not much.
            1: Total path cost calculations are mostly right, no reverse path finding.
            2: Total path cost calculations are correct, but reverse path finding does not work.
            3: Cheapest path is found quickly in some cases, or another correct path is found in all cases. 
            4: Cheapest path is found quickly in all cases.


# Rubrics:
# - 1x shows the total score for a correct path for each of the test cases without jumps (within 1 minute each)
# - 1x displays a correct path for each of the test cases without jumps (within 1 minute each)
# - 1x displays the cheapest path for each of the test cases without jumps (within 1 minute each)
# - 1x displays the cheapest path for each of the test cases with jumps (within 1 minute each)
# - 1x unit tests
# - ?x uses the right data structures
#   - something like a heap or a tree for keeping track of the best square to explore from
#   - a dict to find jumps
#   - a dict or a 2D array to store total costs for each discovered square
# - ?x code quality!

