# This module implements functionality to read the procedure history stored in a a CSV file.
# After the file has been processed the data is returned in a proper data structure.