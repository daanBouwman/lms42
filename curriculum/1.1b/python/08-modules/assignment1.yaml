- |
    Today we are going to work on modules. A module is a file consisting of Python code. A module can define functions and variables and include runnable code. Let's see how we can create more readable code by using modules.
-   Poetry:
    -   Packages?: |
            Good programmers are lazy programmers. There's no reason to implement something that has already been implemented (better) by someone else. There third-party implementations are usually in the form of libraries, or packages. There are many [Open Source](https://opensource.com/resources/what-open-source) libraries freely available, that'll help you build all sorts of programs.

            However, using these packages in Python can be a bit tricky. The standard tool for installing packages is called _pip_. Unfortunately this tool has some drawbacks, of which the main one is that all packages are installed in a global library. This means that if you have two separate projects that use the same package but require different versions, things tend to break.
            
            In order to solve this problem Python created something called 'virtual environment' (_venv_ for short). For each project you can create a separate _venv_ in order to keep the packages (and their version) separate between projects. While _pip_ en _venv_ solve the problem of isolating used packages in project it can be a pain to set up properly. A more user-friendly (and more feature rich) alternative we will use is called Poetry.
    - 
        link: https://modelpredict.com/python-dependency-management-tools#all-solutions-compared
        title: Overview of python dependency management tools
        info: The world of Python dependency management is a mess, but once you understand the tools and why they exist, it’s going to be easier to choose the one you want and deal with the others in environments where you can’t choose your favorite ones.

    - Installing Poetry: |
        As Poetry is an official Manjaro package, you can install it using *Add/remove software*. It's called `python-poetry`.

        After that, you'll need to issue the following command in your terminal, in order to allow VisualStudio Code to play nice with Poetry. You'll only need to do this once:

        ```sh
        poetry config virtualenvs.in-project true
        ```

    - Getting started with Poetry: |
            In order to manage a project's dependencies using Poetry, you first need to initialize it. From within your project's main directory type:
            ```
            $ poetry init -n
            ```

            When you want to run an application in the poetry virtual environment (where the packages are installed) you can use the following command:

            ```
            $ poetry run python main.py
            ```

            For now, this does the same as just running `python main.py`, as we're not using any Poetry-installed packages yet. We'll get to that later.

            Alternatively you can start the poetry shell first - this should only be done once - and afterwards use the regular command to start the application:
            ```
            $ poetry shell
            $ python main.py
            $ python main.py # and run it again!
            ```
    - 
        link: https://www.youtube.com/watch?v=V7UhzA4g2yg&t=140s
        title: Using Poetry to manage Python projects
        info: Poetry provides an all-in-one tool for setting up projects, including virtual environments, dependency management, and many other moving parts. Learn the basics in our five-minute introduction.
   

-   Features:
    -   Import the procedure history: 
        -
            text: |
                Provided with the assignment is a CSV file containing the procedure history of pets. The data contains the details of the owners, pets and the procedures they have had at the vet. Also provided are two files (_main.py_ and _file_reader_module.py_) which you can use to implement the work for the assignment.
                
                The first step is to read the contents from the CSV file and store it into a proper data structure. Implement the logic for the processing of the CSV file in the _file_reader_module.py_ file. The main logic of your program should be implemented in _main.py_.
                
                At this point your main application should look like this:
                ```
                *** Little Paws Veterinary Administration ***
                Enter the name of the file containing your procedure history (type 'exit' to quit the application): nosuchfile
                Cannot open file.
                Enter the name of the file containing your procedure history (type 'exit' to quit the application): vet-procedures.csv
                
                298 procedures.
                ```
            ^merge: feature
        
    -   Compute statistics: 
        -
            text: |
                Compute the statistics over the imported data. Compute the following:
                - The average age of all pets
                - The maximum age of all pets
                - The average procedure price for all given procedures.
                - The daily totals for the procedures (how much was paid on a particular day on procedures).
                
                Be sure to separate the computation of the statistics in a separate file.

                ```
                *** Little Paws Veterinary Administration ***
                Enter the name of the file containing your procedure history (type 'exit' to quit the application vet-procedures.csv
                
                298 procedures.

                [Menu]
                 1. Compute statistics
                 2. Filter the data
                 3. Clear filter
                Please select an option: 1

                [Pet statistics]
                 Cat: Average age 6.79 - Maximum age: 14 - Average procedure price: 36.9 euro
                 Dog: Average age 7.2 - Maximum age: 14 - Average procedure price: 104.91 euro
                 Parrot: Average age 7.06 - Maximum age: 11 - Average procedure price: 213.82 euro
                 All pets: Average age 7.07 - Maximum age: 14 - Average procedure price: 112.23 euro

                [Daily totals]
                 21/12/2016: 40.0 euros
                 06/07/2016: 40.0 euros
                 08/05/2016: 125.0 euros
                 19/08/2016: 40.0 euros
                 02/02/2016: 85.0 euros
                 29/01/2016: 85.0 euros
                 30/07/2016: 170.0 euros
                 ...
                 21/08/2016: 5938.0 euros
                 03/10/2016: 5938.0 euros

                [Menu]
                ```
            ^merge: feature

    -   Implement a filter function: 
        - 
            text: |
                The list of records should be filtered by pet kind or pet name. 
                ```
                *** Little Paws Veterinary Administration ***
                Enter the name of the file containing your procedure history (type 'exit' to quit the application): vet-procedures.csv

                298 procedures.

                 1. Compute statistics
                 2. Filter the data
                 3. Clear filter
                Please select an option: 2

                How do you wish to filter the data?
                 1. By pet kind
                 2. By pet name
                Please select an option: 1

                Select one of the following kind:
                 1. Dog
                 2. Cat
                 3. Parrot
                Please select an option: 1

                161 procedures.

                [Menu]
                 1. Compute statistics
                 2. Filter the data
                 3. Clear filter
                Please select an option: 1

                [Pet statistics]
                 Dog: Average age 7.2 - Maximum age: 14 - Average procedure price: 104.91 euro
                 All pets: Average age 7.2 - Maximum age: 14 - Average procedure price: 104.91 euro

                [Daily Totals]
                06/07/2016: 40.0 euros
                08/05/2016: 125.0 euros
                19/08/2016: 40.0 euros
                04/05/2016: 85.0 euros
                ...
                10/07/2016: 85.0 euros
                30/07/2016: 85.0 euros
                02/08/2016: 1767.0 euros
                30/01/2016: 1767.0 euros
                22/10/2016: 5938.0 euros
                21/08/2016: 5938.0 euros

                161 procedures.

                [Menu]
                 1. Compute statistics
                 2. Filter the data
                 3. Clear filter
                Please select an option: 3

                298 procedures.

                [Menu]
                ```
                - Create a menu option for filtering the data. When the users wishes to filter the data the application should ask the user whether the data should be filtered by kind or name.
                - If the user wishes to filter the data based on a pet kind the application should present a list of available pet kinds (so not a hardcoded list).
                - If the user wishes to filter the data by pet name then the application should search through all pet names that partially match the search keyword.
                - The statistics should be computed over the filtered set.
                - The application should allow the user to clear the filter.

            ^merge: feature

    -   Generate a bar chart:
        -
            link: https://python-poetry.org/docs/cli/#add
            title: Poetry commands - add
            info: The add command adds required packages to your pyproject.toml and installs them.
        -
            link: https://pythonspot.com/matplotlib-bar-chart/
            title: Matplotlib Bar chart
            info: Matplotlib may be used to create bar charts.
        - 
            text: |
                Use the pyplot package to generate a bar chart of the daily sales. Add the package - the matplotlib package - to your project and look up how to create a bar chart using this library. On the x-axis of the chart the date should be plotted and on the y-axis the daily sales (the sum of all the price for all the procedures executed that day). Save the chart as a PNG file.

                In your application the user should have the option (in its menu) to generate a bar chart based on the currently selected data. This can be all the data imported from the file or data that has been filtered (by pet name or kind). 
            ^merge: feature

    -   Convert the application to a CLI application:
        -
            link: https://stackabuse.com/generating-command-line-interfaces-cli-with-fire-in-python/
            title: Generating Command-Line Interfaces (CLI) with Fire in Python
            info: Python Fire is a library for automatically generating command line interfaces (CLIs) from absolutely any Python object or function. Remember to use `poetry` instead of `pip`, so other developers working on your code will also have (the same version of) Fire installed.
        - 
            text: |
                Use _Python Fire_ to convert you application into a command line application which can process files without requiring user interaction. 
                
                Up to this point your application requires user interaction (input for the filename and filter options). The objective here is to provide similar functionality from the command line without requiring user interaction.
                
                Create a new python file called `main_cli.py`. If the user starts this application from the command line with a filename as an argument then your application should read the file, process the data, generate a bar chart and store the chart as an PNG file. Without any `input()`s. You can overwrite the PNG file every time your application runs this way. So in order to generate `graph.png`, you should be able to run:
                
                ```sh
                poetry run python main_cli.py vet-procedures.csv
                ```

                Your CLI application should also accept the following options, for setting the name of the output file and filtering by pet kind and/or a part of the name:

                ```sh
                poetry run python main_cli.py vet-procedures.csv --output=my_dog_graph.png --kind=dog --name=les
                ```
                
                The amount of code in `main_cli.py` should be kept to a minimum by importing your existing modules (and possibly extracting some of the functionality you need into functions that can be used by both `main.py` and `main_cli.py`).

            ^merge: feature
            weight: 0.5
