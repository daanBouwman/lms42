Important notes: |
    - When creating your database be sure to use `NOT NULL` constraint when applicable and automatically generate ids for your primary keys.
    - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
    - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to delete Frank, you can't do something like `delete from teachers where id=42` but you need to do something like `delete from teachers where name='Frank'`.

Create a database:
    - |
        Your assignment is to create a database for registering police incidents. 

    -
        map:
            schemas: 0.5
            queries: 0.5
        text: |
            Within `assignment.md`, write the queries to create a temporary table that matches the `incidents.csv` CSV file, and then to import the CSV data into it. Remember you need to use `../../../incidents.csv` as the file name. Empty fields in the CSV file should be interpreted as `NULL` values.
        1: A sensible start
        2: Partially correct
        3: Correct but sloppy
        4: Correct

    -
        map:
            datamodeling: 1
        text: |
            Study the data in your temporary table and the *logical* ERD (Entity Relationship Diagram) provided in the template using PlantUML. Convert the *logical* ERD into a *physical* ERD. We expect to see data types, primary keys, foreign keys and junction tables.

            Note that we don't know the location for some of the incidents, in which case all of the location fields are unset.
        1: +1 90% correct data types
        2: +1 80% correct foreign/primary keys
        3: +1 mostly correct junction tables
        4: +1 no errors
    
    -
        map:
            schemas: 1
            foreignkeys: 1
        text: |
            Within `assignment.md`, write the queries to create well-formed normalized tables for the incident data, as modeled in your ERD.
        4: +1 normalized, +1 proper names, +1 proper data types (incl NOT NULL), +1 proper keys and auto increments

    -
        map:
            queries: 1
            joins: 1
        text: |
            Within `assignment.md`, write the queries required to insert all data into the normalized tables from the temporary table.

            *Hint:*
            - Remember that multiple agents and multiple suspects with the same name may exist. Badge numbers should be unique though. And you may also assume that a name + birth date combination is unique.
            - If a query is too slow (it times out after 60 seconds), try using `JOIN`s instead of subqueries.
        1: A sensible start
        2: Partially correct
        3: Correct but sloppy
        4: Correct

    - |
        <div class="notification is-important">In case you're unsure about the database schema you created in the objective above, ask a classmate or a teacher to take a look at it before you continue with the rest of the objectives.</div>


Insert more data:
    ^merge: queries
    map:
        queries: 1
        joins: 1


Query the data:
    ^merge: queries
    map:
        queries: 1
        joins: 1
        aggregate: 1
        subqueries: 2

Change the data:
    ^merge: queries
    map:
        queries: 1
        joins: 1
        subqueries: 1

Create and use a view:
    ^merge: queries
    map:
        views: 1

Make it fast:
    map:
        indexes: 1
    text: |
        Provide the SQL for creating indexes that may help speed up your queries in answer to *6.1* and *6.2*. You should assume that all of your database tables contain many millions of rows (as may well be the case when the database has been in use for a decade or two).

    3: -1 per unnecessary index
    4: +2 per correct index


Extend the design:
    map:
        datamodeling: 1
    text: |
        Within `assignment.md`, create a copy of your ERD from objective #2, and extend it in order to keep track of how incidents are handled in court by judges:

        - Every incident *can* be associated with one court case. A single court case can handle more than one incident. A case has a case number, a registration date, a ruling date and the height of the fine (which is only set after ruling has taken place).
        - Every case will be presented to a judge. For each judge we want to know the name and the date when he or she was appointed. A judge works for *one* court, but each court can have many judges working of it. Of a court we know its name and location.
        - On a case one or more lawyers are involved. The name and graduation date for each lawyer should be stored.
        
    0: No design provided.
    1: Incomplete and/or faulty design.
    2: Half of the entities relationships are correct.
    3: Almost all entities and relationships are correct.
    4: All entities and relationships are correct.
