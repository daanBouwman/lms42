a: int2 0
b: int2 0 # test 123
hello: "Hello \"world!"

main:
set2 a b
set2 a 123
add2 a b
add2 a 123
sub2 a b
set2 a *lala
set2 *lala 1
set2 a *123

lala:
my_data:
set2 a &my_data
set2 a my_data
set2 my_data a

address:
if>2 a b set2 ip &address
if=2 a 0 set2 ip &address

read2 a
write2 a
# never mind this


1 byte: opcode
	0: exit
	1: set
	2: add
	3: sub
	4: mul
	5: div
	6: logic and
	7: logic or
	8: logic xor
	9: if= # skip next instruction unless true
	10: if>
	11: if<
	12: read # read a byte (0 through 255) from stdin and put it at dst. -1 on end-of-file.
	13: write # write the byte value at src to stdout. While src is actually 2 bytes, only the lower byte is written.
	14-255: reserved for future use
1 byte: argument details
	bits 0-1: word size
		0: 1 byte (unsigned)
		1: 2 bytes (signed)
		2: 4 bytes (signed)
		3: reserved
	bits 2-3: arg1 type
		0: the value is 0; arg1 omitted
		1: the value is stored as the arg1 (it's a plain value)
		2: the value should be retrieved from the address indicated by the arg1 (it's a pointer)
		3: the value should be retrieved from the address indicated by the data at the address indicated by the destinations data (it's a pointer to a pointer)
	bits 4-5: arg2 type
		0: the value is 0; arg2 omitted
		1: the value is stored as the arg2 (it's a plain value)
		2: the value should be retrieved from the address indicated by the arg2 (it's a pointer)
		3: the value should be retrieved from the address indicated by the data at the address indicated by the sources data (it's a pointer to a pointer)
	bits 6-7: reserved
0, 1, 2 or 4 bytes: arg1 (0 if arg1 type is 0, word size if arg1 type is 1, otherwise 2 (a memory address))
0, 1, 2 or 4 bytes: arg2 (0 if arg2 type is 0, word size if arg1 type is 1, otherwise 2 (a memory address))
