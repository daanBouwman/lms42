# SaxTP server and reference client

## Installation

- Make sure you have a recent `node.js` and `npm` installed
- In this directory run: `npm install`

## Server usage

- In this directory run: `npm start`
- Files in the `public` subdirectory are served
- Make sure UDP port 29588 is exposed to any clients you want to allow

## Client usage
- In this directory run: `node client.js <hostname-or-ip> <filename>`
- The application will attempt to download the file to the current directory
- It will terminate if (and only if) the download was completed successfully

